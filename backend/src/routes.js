const express = require('express');
const axios = require('axios');
const routes = express.Router();
const OngController = require('./controllers/ongConrtroller');
const IncidentController = require('./controllers/incidentController');
const ProfileController = require('./controllers/profileController');
const SessionController = require('./controllers/sessionController');

routes.post('/session', SessionController.criar);

routes.get('/ongs', OngController.listar);
routes.post('/ongs', OngController.criar);

routes.get('/incidents', IncidentController.listar);
routes.post('/incidents', IncidentController.criar);
routes.delete('/incidents/:id', IncidentController.deletar);

routes.get('/profile', ProfileController.listar);

routes.get('/teste-api-externa', async function(request, response) {
  await axios
    .post('http://api.carfacts.com.br/v1/auth/login', {
      email: 'jociandro.coelho@gmail.com',
      senha: 'SNFREITAS2020'
    })
    .then(res => {
      response.json(res.data);
    })
    .catch(error => {
      response.json(error);
    });
});

module.exports = routes;
