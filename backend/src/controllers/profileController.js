const conn = require('../database/connections');

module.exports = {
  async listar(request, response) {
    const ong_id = request.headers.authorization;

    const incident = await conn('incidents')
      .where('ong_id', ong_id)
      .select('*');

    return response.json(incident);
  }
};
