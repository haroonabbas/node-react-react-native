const conn = require('../database/connections');

module.exports = {
  async criar(request, response) {
    const { titulo, descricao, valor } = request.body;
    const ong_id = request.headers.authorization;

    const [id] = await conn('incidents').insert({
      titulo,
      descricao,
      valor,
      ong_id
    });

    return response.json({ id });
  },
  async listar(request, response) {
    const { page = 1 } = request.query;
    const [count] = await conn('incidents').count();
    const totalPages = Math.ceil(Number(count['count(*)']) / 5);

    const incidents = await conn('incidents')
      .join('ongs', 'ongs.id', '=', 'incidents.ong_id')
      .limit(5)
      .offset((page - 1) * 5)
      .select([
        'incidents.*',
        'ongs.nome',
        'ongs.email',
        'ongs.whatsapp',
        'ongs.cidade',
        'ongs.uf'
      ]);

    response.header('X-Total-Count', count['count(*)']);
    response.header('X-Total-Pages', totalPages);

    return response.json(incidents);
  },

  async deletar(request, response) {
    const { id } = request.params;
    const ong_id = request.headers.authorization;

    const incident = await conn('incidents')
      .where('id', id)
      .select('ong_id')
      .first();

    if (incident.ong_id !== ong_id) {
      return response.status(401).json({ error: 'Operação não permitida' });
    }

    await conn('incidents')
      .where('id', id)
      .delete();

    return response.status(204).send();
  }
};
